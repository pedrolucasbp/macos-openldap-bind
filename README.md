## macOS-openldap-bind
This script creates the necessary configuration to connect the MAC OS to an LDAP Server. 
The configuration is made from a template file plus some information provided by the user as parameters.
For more information on how to create your own template look at "Obtaining the template file".
Feel free to adapt to yours needs.
    
#### Versions
-macOS 10.13 or later

-OpenLDAP 2.4
        
#### Sources
-https://github.com/rtrouton/rtrouton_scripts/tree/master/rtrouton_scripts/open-ldap_bind_script

-https://docs.foxpass.com/docs/mac-os-x-logins-over-ldap

## Bind to LDAP Server:

    sudo bash ./macos-domain-bind.sh bind \
    -n example.com \
    -s dir.example.com \
    -b "dc=example,dc=com" \
    -u ou=users \
    -g ou=groups \
    -B "cn=binduser,dc=example,dc=com" \
    -P 'xxx'


## Testing LDAP bind:

    dscacheutil -flushcache

    dsmemberutil getuuid -U <DeinLoginName>

    dscacheutil -q user -a name <DeinLoginName>

    dscl localhost -list /LDAPv3


## Unbind the LDAP Server:

    sudo bash ./macos-domain-bind.sh unbind \
    -n example.com 


## Manually restoring:

    sudo dscl /Contacts delete / CSPSearchPath /LDAPv3/example.com
    sudo dscl /Search delete / CSPSearchPath /LDAPv3/example.com
    sudo rm -fr /Library/Management/
    sudo rm -fr /Library/Preferences/OpenDirectory/Configurations/LDAPv3/
    sudo /usr/bin/security delete-generic-password -l /LDAPv3/example.com /Library/Keychains/System.keychain
    sudo /bin/launchctl stop com.apple.opendirectoryd
    sudo /bin/launchctl start com.apple.opendirectoryd
    sudo defaults delete com.apple.loginwindow LoginHook
    sudo defaults delete com.apple.loginwindow DisableFDEAutologin
    sudo defaults delete com.apple.loginwindow LoginwindowText


# Obtaining the template file

    /usr/libexec/PlistBuddy -x -c "Print" \
    /Library/Preferences/OpenDirectory/Configurations/LDAPv3/example.com.plist \
    > data/directory-server-bind-config-template.plist

\*This file must be edited manually in order to adapt the configuration fields to the processing of the script.

