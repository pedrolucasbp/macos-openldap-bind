#!/bin/bash

LOGFILE=/var/log/loginhook.log
CREATEMOBILEACCOUNTTOOL="/System/Library/CoreServices/ManagedClient.app/Contents/Resources/createmobileaccount"

echo "$(date) Login hook executed for user $1" >> $LOGFILE

if [ ! -z "$1" ] && [ "_mbsetupuser" != "$1" ]; then
    echo "$(date) First Login from user $1" >> $LOGFILE

    #so far this command does not work well on the first login
    #echo "Configuring Mobile Account"  >> $LOGFILE
    #$CREATEMOBILEACCOUNTTOOL -n $1 -h /Users/$1 >> $LOGFILE
fi
