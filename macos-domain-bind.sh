#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# This script creates the necessary configuration to connect the MAC OS to an 
# LDAP Server. [MAC OS 10.13 or later; OpenLDAP 2.4].
# The configuration is made from a template file plus some information provided 
# by the user as parameters.
# For more information on how to create your own template look at README.txt
# Feel free to adapt to yours needs.

# This script was based on rtrouton Mac_OpenLDAP_bind_script.sh , URL:
# https://github.com/rtrouton/rtrouton_scripts/tree/master/rtrouton_scripts/open-ldap_bind_script
# and
# https://docs.foxpass.com/docs/mac-os-x-logins-over-ldap
# Author Pedro Lucas <pedrolucasbp at gmail dot com>
# v 1.0

# LOG FILE
LOGFILE="/var/log/macos-ldap-bind.log"

# WORK SPACE
TEMPDIR="/tmp/$RANDOM-macos-domain-bind/"

# MAC OS CONFIGURATION INFOS
LDAPCONFDIR="/Library/Preferences/OpenDirectory/Configurations/LDAPv3/"
SEARCHCONFDIR="/Library/Preferences/OpenDirectory/Configurations/"
LOGINMNGDIR="/Library/Management/"
SYSKEYCHAIN="/Library/Keychains/System.keychain"

# INTERNAL TOOLS
TOOLSDIR="tools/"
FIRSTLOGINSCRIPT="login-script.sh"

# INTERNAL DATA
DATADIR="data/"
SERVERBINDCONFTPT="directory-server-bind-config-template.plist"

# New Domain settings
NEWDOMAINNAME="example.com" 	                 # Name of configuration
NEWDOMAINSERVER="dir.example.com"                # FQDN from Server
NEWDOMAINSBASEDN="dc=example,dc=com"             # Base DN from LDAP Search
NEWDOMAINSUSERSOU="ou=users"                     # Users OU in LDAP Server
NEWDOMAINSGROUPSOU="ou=groups"                   # Groups OU in LDAP Server
NEWDOMAINSBINDDN="cn=binduser,dc=example,dc=com" # BindDN  
NEWDOMAINSBINDDNPASS="xxxx"                      # Password from BindDN

function usage(){
    echo "usage:"
    echo "$0 [command] <options>"
    echo -e "\tbind"
    echo -e "\t\t-n \"Domain Name\" e.g., \"$NEWDOMAINNAME\""
    echo -e  "\t\t-s \"FQDN from LDAP Server\" e.g., \"$NEWDOMAINSERVER\""
    echo -e  "\t\t-b \"Base DN from LDAP Search\" e.g., \"$NEWDOMAINSBASEDN\""
    echo -e  "\t\t-u \"Users OU in LDAP Server\" e.g., \"$NEWDOMAINSUSERSOU\""
    echo -e  "\t\t-g \"Groups OU in LDAP Server\" e.g., \"$NEWDOMAINSGROUPSOU\""
    echo -e  "\t\t-B \"BindDN\" e.g., \"$NEWDOMAINSBINDDN\""
    echo -e  "\t\t-P \"Password from BindDN\" e.g., \"$NEWDOMAINSBINDDNPASS\""
    echo ""
    echo -e "\tunbind"
    echo -e "\t\t-n \"Domain Name\" e.g., \"$NEWDOMAINNAME\""
    echo -e "\t\t This Domain Name must be configured in the local Directory Service."
    echo ""
    
}

# For now we need root 
function prerequisites(){
    > $LOGFILE # Recreating LOGFILE
    echo "Looking for prerequisites..." | tee -a $LOGFILE
    if [ "$EUID" -ne 0 ]; then
        echo "ERROR: prerequisites not met." | tee -a $LOGFILE
        echo -e "\tThis script need root privileges!" | tee -a $LOGFILE
        exit 1
    fi
    echo "ok" | tee -a $LOGFILE
}

function bind(){
    prerequisites
    echo "Lets go!" | tee -a $LOGFILE
    echo "Trying to bind this MAC OS to $NEWDOMAINNAME" | tee -a $LOGFILE

    echo "Preparing files..." | tee -a $LOGFILE

    mkdir -p $TEMPDIR

    if [ $? -eq 0 ]; then
        
        cp -v $DATADIR$SERVERBINDCONFTPT $TEMPDIR
        if [ $? -eq 0 ]; then 
            # PROCESSING MAIN FILE
            
            # DIRECTORY SERVICE LDAPV3 CONFIG
            sed -i -e "s/NEWDOMAINNAME/$NEWDOMAINNAME/g" $TEMPDIR/$SERVERBINDCONFTPT
            sed -i -e "s/NEWDOMAINSERVER/$NEWDOMAINSERVER/g" $TEMPDIR/$SERVERBINDCONFTPT
            sed -i -e "s/NEWDOMAINSBASEDN/$NEWDOMAINSBASEDN/g" $TEMPDIR/$SERVERBINDCONFTPT
            sed -i -e "s/NEWDOMAINSUSERSOU/$NEWDOMAINSUSERSOU,$NEWDOMAINSBASEDN/g" \
            $TEMPDIR/$SERVERBINDCONFTPT
            sed -i -e "s/NEWDOMAINSGROUPSOU/$NEWDOMAINSGROUPSOU,$NEWDOMAINSBASEDN/g" \
            $TEMPDIR/$SERVERBINDCONFTPT
            sed -i -e "s/NEWDOMAINSBINDDN/$NEWDOMAINSBINDDN/g" $TEMPDIR/$SERVERBINDCONFTPT
            
            # The Password is stored in System keychain.
            #
            /usr/bin/security add-generic-password \
            -a "$NEWDOMAINSBINDDN" \
            -w "$NEWDOMAINSBINDDNPASS" \
            -s "/LDAPv3/$NEWDOMAINNAME" \
            -l "/LDAPv3/$NEWDOMAINNAME" \
            -A $SYSKEYCHAIN
        else
            echo "ERROR: impossible to copy temporary files." | tee -a $LOGFILE
            exit 3
        fi
    else
        echo "ERROR: impossible to create temporary directory." | tee -a $LOGFILE
        exit 2
    fi
    echo "Done." | tee -a $LOGFILE

    echo "Configuring LDAP..." | tee -a $LOGFILE

    DSLDAPCONF="$(dscl localhost -list /LDAPv3)" 
    if [ -n $DSLDAPCONF  ]; then
        echo "Existings LDAP bindings: " | tee -a $LOGFILE
        echo "$DSLDAPCONF"
    fi

    echo "Binding to LDAP Domain $NEWDOMAINNAME" | tee -a $LOGFILE

    if [ -f $LDAPCONFDIR$NEWDOMAINNAME.plist ]; then
        echo -e "\tThis configuration already exists!" | tee -a $LOGFILE
        echo -e "\tMoving to $TEMPDIR/old-ldap-config-$NEWDOMAINNAME.plist" | tee -a $LOGFILE
        mv -v $LDAPCONFDIR$NEWDOMAINNAME.plist $TEMPDIR/old-config-$NEWDOMAINNAME.plist | tee -a $LOGFILE

        echo "Removing previous search bindings..." | tee -a $LOGFILE
        dscl /Search/Contacts delete / CSPSearchPath /LDAPv3/$NEWDOMAINNAME
        if [ $? -ne 0 ]; then
            echo "ERROR: can't delete previous Contacts path." | tee -a $LOGFILE
            exit 4
        fi
        dscl /Search delete / CSPSearchPath /LDAPv3/$NEWDOMAINNAME
        if [ $? -ne 0 ]; then
            echo "ERROR: can't delete previous Search path." | tee -a $LOGFILE
            exit 4
        fi
        echo "ok" | tee -a $LOGFILE
    fi

    if [ ! -d  $LDAPCONFDIR ]; then
        mkdir -p $LDAPCONFDIR
    fi

    echo "Moving main configuration file to opendirectoryd configuration directory..." | tee -a $LOGFILE

    mv -v $TEMPDIR$SERVERBINDCONFTPT $LDAPCONFDIR$NEWDOMAINNAME.plist
    if [ $? -ne 0 ]; then
        echo "ERROR: can't move the new configuration file to $LDAPCONFDIR." | tee -a $LOGFILE
        exit 5
    fi  
    echo "ok" | tee -a $LOGFILE

    echo "Restarting opendirectoryd..." | tee -a $LOGFILE

    /bin/launchctl stop com.apple.opendirectoryd

    if [ $? -ne 0 ]; then
        echo -e "\tERROR: opendirectoryd not stoping!" | tee -a $LOGFILE
        exit 5
    fi

    # Give DS a chance to catch up
    sleep 5

    /bin/launchctl start com.apple.opendirectoryd
    if [ $? -ne 0 ]; then

        echo -e "\tERROR: opendirectoryd not starting!" | tee -a $LOGFILE
        exit 6
    fi
    echo "ok" | tee -a $LOGFILE

    echo "Merging the new search path..." | tee -a $LOGFILE

    dscl /Search merge / CSPSearchPath /LDAPv3/$NEWDOMAINNAME
    if [ $? -ne 0 ]; then
        echo "ERROR: can't merge new search path." | tee -a $LOGFILE
        exit 7
    fi
    dscl /Search/Contacts merge / CSPSearchPath /LDAPv3/$NEWDOMAINNAME
    if [ $? -ne 0 ]; then
        echo "ERROR: can't merge new Contacts path." | tee -a $LOGFILE
        exit 7
    fi

    echo "Finished Binding." | tee -a $LOGFILE

    echo "Configuring LDAP users logins..." | tee -a $LOGFILE

    echo "Linking /home to /Users..." | tee -a $LOGFILE
    if [ ! -L /home ]; then

        HOMECONTENT="$(ls -l /home/)"
        
        if [ -z $HOMECONTENT ]; then
        
            rm -fr /home
            ln -sv /Users /home
            
        else 
        
            echo "ERROR: /home is not empty!" | tee -a $LOGFILE
            exit 8
        fi
        
    else

        ls -l /home | tee -a $LOGFILE
        echo "/home is a symbolic link" | tee -a $LOGFILE
        
    fi

    echo "Configuring LOGIN MANAGEMENT DIRECTORY..." | tee -a $LOGFILE
    if [ ! -d $LOGINMNGDIR ]; then

        mkdir $LOGINMNGDIR

        if [ $? -ne 0 ]; then
            echo "ERROR: can't create LOGIN MANAGEMENT DIRECTORY." | tee -a $LOGFILE
            exit 9
        fi
    fi

    cp -v $TOOLSDIR$FIRSTLOGINSCRIPT $LOGINMNGDIR

    if [ $? -ne 0 ]; then
        echo "ERROR: can't copy LOGIN SCRIPT." | tee -a $LOGFILE
        exit 10
    fi

    chown root:wheel $LOGINMNGDIR$FIRSTLOGINSCRIPT
    chmod 755 $LOGINMNGDIR$FIRSTLOGINSCRIPT


    echo "Configuring loging Hook..." | tee -a $LOGFILE
    defaults write /Library/Preferences/com.apple.loginwindow\
    LoginHook $LOGINMNGDIR$FIRSTLOGINSCRIPT
    if [ $? -ne 0 ]; then
        echo "ERROR: can't set $LOGINMNGDIR$FIRSTLOGINSCRIPT as hook in loginwindow." | tee -a $LOGFILE
        exit 11
    fi

    echo "Configuring Login Window to show..." | tee -a $LOGFILE
    defaults write /Library/Preferences/com.apple.loginwindow\
    DisableFDEAutologin -bool YES
    if [ $? -ne 0 ]; then
        echo "ERROR: can't set com.apple.loginwindow DisableFDEAutologin -bool YES" | tee -a $LOGFILE
        exit 12
    fi

    echo "Configuring a message in Login Window..." | tee -a $LOGFILE
    defaults write /Library/Preferences/com.apple.loginwindow  LoginwindowText\
     -string "$HOSTNAME @ $NEWDOMAINNAME"
    if [ $? -ne 0 ]; then
        echo "WARNING: can't set com.apple.loginwindow LoginwindowText" | tee -a $LOGFILE
    fi
    echo "Done." | tee -a $LOGFILE

    echo "Success." | tee -a $LOGFILE
}

# Here we don't need ERRORs because the administrator
# can see what didn't work and can do it manually :/ 
function unbind(){
    prerequisites
    echo "Unbinging as you like.." | tee -a $LOGFILE
    echo "Trying to removing the configurations from $NEWDOMAINNAME " | tee -a $LOGFILE
    isdoimainconfigured="$(dscl localhost -list /LDAPv3 | grep $NEWDOMAINNAME)"
    if [[ $isdoimainconfigured =~ "$NEWDOMAINNAME" ]]; then
        dscl /Contacts delete / CSPSearchPath /LDAPv3/$NEWDOMAINNAME
        dscl /Search delete / CSPSearchPath /LDAPv3/$NEWDOMAINNAME
        rm -fr /Library/Management/
        rm -fr /Library/Preferences/OpenDirectory/Configurations/LDAPv3/
        /usr/bin/security delete-generic-password -l /LDAPv3/$NEWDOMAINNAME $SYSKEYCHAIN
        /bin/launchctl stop com.apple.opendirectoryd
        /bin/launchctl start com.apple.opendirectoryd
        defaults delete /Library/Preferences/com.apple.loginwindow LoginHook
        defaults delete /Library/Preferences/com.apple.loginwindow DisableFDEAutologin
        defaults delete /Library/Preferences/com.apple.loginwindow LoginwindowText
    else
        echo "But you don't have the Domain $NEWDOMAINNAME in yours System." | tee -a $LOGFILE
        echo "Nothing to do here." | tee -a $LOGFILE
    fi
}

if [ $# -eq 0 ];then
    usage
    exit 1
fi

while getopts ":h" opt; do
  case ${opt} in
    h ) # Help
        usage
        exit 0
        ;;
    esac
done

shift $((OPTIND -1))
subcommand="$1"
shift

case "$subcommand" in
    bind )
        while getopts ":n:s:b:u:g:B:P:" opt; do
            case ${opt} in
                n ) # Name of configuration
                    echo "$OPTARG"
                    NEWDOMAINNAME="$OPTARG"
                    ;;
                s ) # FQDN from Server
                    NEWDOMAINSERVER="$OPTARG"
                    ;;
                b ) # Base DN from LDAP Search
                    NEWDOMAINSBASEDN="$OPTARG"
                    ;;
                u ) # Users OU in LDAP Server
                    NEWDOMAINSUSERSOU="$OPTARG"
                    ;;
                g ) # Groups OU in LDAP Server
                    NEWDOMAINSGROUPSOU="$OPTARG"
                    ;;
                B ) # BindDN
                    NEWDOMAINSBINDDN="$OPTARG"
                    ;;
                P ) # Password from BindDN
                    NEWDOMAINSBINDDNPASS="$OPTARG"
                    ;;
            esac
        done
        bind
        shift $((OPTIND -1))
        ;;
    unbind )
        while getopts ":n:" opt; do
            case ${opt} in
                n ) # Name of configuration
                    NEWDOMAINNAME="$OPTARG"
                    ;;
                : ) # invalid arg
                    ;;
            esac
        done
        unbind
        shift $((OPTIND -1))
        ;;
    
esac

echo "This MAC is now bound to Domain: " | tee -a $LOGFILE
echo ""
dscl localhost -list /LDAPv3 | tee -a $LOGFILE
echo ""

echo "With Search and Contacts Path entries: " | tee -a $LOGFILE
dscl /Search -read / CSPSearchPath | tee -a $LOGFILE
exit 0                    ## Success
